package org.deroesch.grp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Main {

	/*
	 * Parsing JSON in Java is still painful.
	 */
	public static void main(String[] args) throws IOException {
		
		// The big, bad JSON parser.
		Gson g = new Gson();
		
		// Load sample JSON text from a file.
		String json = new String(Files.readAllBytes(Paths.get("data/array.json")));
		
		// Convert file contents to a JSON Array.
		JsonElement e1 = JsonParser.parseString(json);
		JsonArray a1 = e1.getAsJsonArray(); 
		
		// Loop through the array, extract each Field Descriptor,
		// convert it to a Java object, then print out the object.
		for (int i = 0 ; i < a1.size(); i++)
		{
			JsonElement e2 = a1.get(i);
			JsonObject o2 = e2.getAsJsonObject();
			
			String s = g.toJson(o2);
			Field fieldDescriptor = g.fromJson(s, Field.class);
			
			System.out.println(fieldDescriptor);
		}
	}
}
