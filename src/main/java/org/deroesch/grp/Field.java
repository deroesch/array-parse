package org.deroesch.grp;

/**
 * A simple class for describing field values.
 */
public class Field {

	private String name;
	private String value;
	private String param_type;

	public Field() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getParam_type() {
		return param_type;
	}

	public void setParam_type(String param_type) {
		this.param_type = param_type;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", value=" + value + ", param_type=" + param_type + "]";
	}
}
